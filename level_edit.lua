local editor = require("polywell")
local lume = require("lume")
local bump = require("bump.bump")
local utils = require("utils")

local thick = 16
local world = bump.newWorld()

local populate_world = function()
   for _,i in pairs(world:getItems()) do world:remove(i) end
   world:add(state.p, utils.xywh(state.p))
   for _,i in pairs(lume.concat(state.platforms, state.ladders,
                                state.tanks, state.pipes)) do
      world:add(i, utils.xywh(i))
   end
end

local find_item = function()
   populate_world()
   local _,_,cols = world:check(state.p, state.p.x, state.p.y)
   return cols[1] and cols[1].other
end

local activate_level_editor = function()
   editor.change_buffer("*level*", true)
   editor.activate_mode("level_edit")
   state.p.x = state.p.x - (state.p.x % 8)
   state.p.y = state.p.y - (state.p.y % 8)
   state.world:update(state.p, state.p.x, state.p.y)
   state.original_message = state.message
   state.message = "Edit mode: ctrl-arrow, alt-arrow, p, l, i, t, e, [, ]"
end

local states = {}

local undo = function()
   local prev_state = table.remove(states, #states)
   if(prev_state) then
      lume.extend(state, lume.deserialize(prev_state))
   end
end

local wrap = function(f, ...)
   if(f ~= undo) then
      local saved_state = lume.pick(state, "level", "platforms",
                                    "ladders", "tanks", "pipes", "start")
      table.insert(states, lume.serialize(saved_state))
      if(#states > 64) then table.remove(states, 1) end
   end
   f(...)
end

editor.define_mode("level_edit", "default", {draw=require("draw").draw,
                                             wrap=wrap})
editor.bind("default", "`", activate_level_editor)
editor.bind("default", "f2", activate_level_editor)

editor.bind("level_edit", "ctrl-z", undo)

-- movement
local update = function(field, dir)
   state.p[field] = state.p[field] + dir
   state.world:update(state.p, state.p.x, state.p.y)
end

local speed = 8
editor.bind("level_edit", "up", lume.fn(update, "y", -speed))
editor.bind("level_edit", "down", lume.fn(update, "y", speed))
editor.bind("level_edit", "left", lume.fn(update, "x", -speed))
editor.bind("level_edit", "right", lume.fn(update, "x", speed))

-- creating things

editor.bind("level_edit", "p", function()
               table.insert(state.platforms,
                            {x=state.p.x, y=state.p.y,w=thick,h=thick,
                             platform=true,}) end)

editor.bind("level_edit", "l", function()
               table.insert(state.ladders,
                            {x=state.p.x, y=state.p.y,w=thick,h=thick,
                             ladder=true,}) end)

editor.bind("level_edit", "t", function()
               table.insert(state.tanks,
                            {x=state.p.x, y=state.p.y, thick=thick, tank=true,
                             w=thick,h=thick,level=0,original_level=0}) end)

editor.bind("level_edit", "i", function()
               table.insert(state.pipes,
                            {x=state.p.x, y=state.p.y,
                             w=thick,h=thick,pipe=true, tanks={}}) end)

-- move item
local hook_up_pipe = function(pipe)
   pipe.tanks = {}
   local _,_,cols = world:check(pipe, pipe.x, pipe.y)
   for _,c in pairs(cols) do
      if(c.other.tank) then
         local tank_num = lume.find(state.tanks, c.other)
         table.insert(pipe.tanks, tank_num)
      end
   end
end

local change = function(field, dir)
   local item = find_item()
   if((not item) or (not item[field])) then return end
   if((field == "w" or field == "h") and item[field] + dir >= 16) then
      item[field] = item[field] + dir
      state.world:update(item, item.x, item.y, item.w, item.h)
   elseif(field == "level" and item.level + dir >= 0 and
          item.level + dir <= item.h - item.thick) then
      item[field] = item[field] + dir
      item.original_level = item.level
   elseif(field == "x" or field == "y") then
      item[field] = item[field] + dir
   end
   if(field == "x" or field == "y") then
      state.p[field] = state.p[field] + dir
      state.world:update(state.p, state.p.x, state.p.y)
   end
   if(item.pipe or item.tank) then
      for _,p in pairs(state.pipes) do
         hook_up_pipe(p)
      end
   end
end

editor.bind("level_edit", "ctrl-up", lume.fn(change, "y", -8))
editor.bind("level_edit", "ctrl-down", lume.fn(change, "y", 8))
editor.bind("level_edit", "ctrl-left", lume.fn(change, "x", -8))
editor.bind("level_edit", "ctrl-right", lume.fn(change, "x", 8))

-- resize
editor.bind("level_edit", "alt-up", lume.fn(change, "h", -8))
editor.bind("level_edit", "alt-down", lume.fn(change, "h", 8))
editor.bind("level_edit", "alt-left", lume.fn(change, "w", -8))
editor.bind("level_edit", "alt-right", lume.fn(change, "w", 8))

-- liquid up/down
editor.bind("level_edit", "[", lume.fn(change, "level", -1))
editor.bind("level_edit", "]", lume.fn(change, "level", 1))

editor.bind("level_edit", "backspace", function()
               local item = find_item()
               lume.remove(state.platforms, item)
               lume.remove(state.tanks, item)
               lume.remove(state.ladders, item)
               lume.remove(state.pipes, item)
end)

editor.bind("level_edit", "s", function()
               state.start = {x = state.p.x, y = state.p.y}
end)
