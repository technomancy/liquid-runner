local polywell = require("polywell")
local fs_for = require("fs")

love.keyreleased = function() end
love.keypressed = polywell.handle_key
love.textinput = polywell.handle_textinput
love.wheelmoved = polywell.handle_wheel

love.run = function()
   love.load()
   while true do
      love.event.pump()
      for name, a,b,c,d,e,f in love.event.wait do
         if(name == "keypressed") then
            love.keypressed(a,b,c,d,e,f)
         elseif(name == "keyreleased") then
            love.keyreleased(a,b,c,d,e,f)
         elseif(name == "textinput") then
            love.textinput(a,b,c,d,e,f)
         elseif(name == "wheelmoved") then
            love.wheelmoved(a,b,c,d,e,f)
         elseif(name == "quit") then
            os.exit()
         end

         love.graphics.clear(love.graphics.getBackgroundColor())
         love.graphics.origin()
         polywell.draw()
         love.graphics.present()
      end
   end
end

love.load = function()
   local multiprint = function(x) print(x) polywell.print(x) end
   love.graphics.setFont(love.graphics.newFont("inconsolata.ttf", 14))
   love.keyboard.setTextInput(true)
   love.keyboard.setKeyRepeat(true)

   local init_file = os.getenv("HOME") .. "/.polywell/init.lua"
   local ok, err = pcall(dofile, init_file)
   if(not ok) then
      multiprint(err)
      multiprint("Using default config.")
      local chunk = assert(love.filesystem.load("config/init.lua"))
      chunk()
   end
   polywell.fs = polywell.fs or fs_for(os.getenv("PWD"))
end
