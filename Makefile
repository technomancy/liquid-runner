check:
	luacheck --no-color --globals fs love state pp -- *.lua

liquid-runner.love: *.lua levels/*.lua Makefile
	find . -name "*.lua" | LC_ALL=C sort | env TZ=UTC zip -r -q -9 -X $@ -@ \
	  assets/*

REL=".love-release/build/love-release.sh"
FLAGS=-a 'Phil Hagelberg' \
	--description 'A puzzle platformer with fluid mechanics.' \
	--love 0.10.1 --url https://technomancy.itch.io/liquid-runner

mac:
	$(REL) $(FLAGS) -M
	mv "releases/Liquid Runner-macosx-x64.zip" releases/liquid-runner-mac.zip

windows:
	$(REL) $(FLAGS) -W -W32
	mv "releases/Liquid Runner-win32.zip" releases/liquid-runner-windows.zip

