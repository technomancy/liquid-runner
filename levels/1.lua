return {
  message = "Liquid Runner! Arrows: move, Space: toggle pipe.",
  ladders = {
    {
      h = 178,
      w = 16,
      x = 256,
      y = 48
    },
    {
      h = 64,
      w = 16,
      x = 224,
      y = 48
    },
    {
      h = 192,
      ladder = true,
      w = 16,
      x = 456,
      y = 0
    }
  },
  level = 1,
  pipes = {
    {
      h = 128,
      open = false,
      tanks = {
        2,
        1
      },
      w = 16,
      x = 112,
      y = 102
    }
  },
  platforms = {
    {
      h = 16,
      thick = 16,
      w = 90,
      x = 400,
      y = 192
    }
  },
  tanks = {
    {
      h = 450,
      level = 70,
      thick = 16,
      w = 520,
      x = 0,
      y = -160
    },
    {
      h = 80,
      level = 62,
      thick = 16,
      w = 200,
      x = 56,
      y = 48
    }
  }
}
