return {
  message = "If you get stuck, use ctrl-r to restart.",
  ladders = {
    {
      h = 56,
      w = 16,
      x = 228,
      y = 196
    },
    {
      h = 120,
      w = 16,
      x = 92,
      y = 52
    },
    {
      h = 24,
      w = 16,
      x = 344,
      y = 192
    },
    {
      h = 208,
      ladder = true,
      w = 16,
      x = 408,
      y = 0
    }
  },
  level = 3,
  pipes = {
    {
      h = 80,
      open = false,
      pipe = true,
      tanks = {
        1,
        2
      },
      w = 16,
      x = 60,
      y = 140
    }
  },
  platforms = {
    {
      h = 16,
      w = 80,
      x = 248,
      y = 248
    },
    {
      h = 48,
      w = 16,
      x = 248,
      y = 200
    },
    {
      h = 16,
      w = 64,
      x = 368,
      y = 200
    },
    {
      h = 16,
      w = 96,
      x = 148,
      y = 52
    },
    {
      h = 64,
      w = 16,
      x = 228,
      y = 4
    }
  },
  start = {
    x = 184,
    y = 32
  },
  tanks = {
    {
      h = 144,
      level = 107,
      original_level = 107,
      tank = true,
      thick = 16,
      w = 112,
      x = 12,
      y = 52
    },
    {
      h = 96,
      level = 4,
      original_level = 4,
      tank = true,
      thick = 16,
      w = 352,
      x = 20,
      y = 196
    }
  }
}
