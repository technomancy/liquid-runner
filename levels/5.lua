return {
  ladders = {
    {
      h = 264,
      ladder = true,
      w = 16,
      x = 356,
      y = -4
    }
  },
  level = 5,
  pipes = {
    {
      h = 96,
      open = false,
      pipe = true,
      tanks = {
        1,
        3
      },
      w = 16,
      x = 220,
      y = 100
    },
    {
      h = 96,
      open = false,
      pipe = true,
      tanks = {
        1,
        2
      },
      w = 16,
      x = 124,
      y = 100
    }
  },
  platforms = {
    {
      h = 16,
      platform = true,
      w = 144,
      x = 236,
      y = 260
    },
    {
      h = 112,
      platform = true,
      w = 16,
      x = 300,
      y = 92
    },
    {
      h = 16,
      platform = true,
      w = 32,
      x = 164,
      y = 180
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 288,
      y = 160
    },
    {
      h = 16,
      platform = true,
      w = 24,
      x = 112,
      y = 260
    }
  },
  start = {
    x = 172,
    y = 156
  },
  tanks = {
    {
      h = 128,
      level = 112,
      original_level = 112,
      tank = true,
      thick = 16,
      w = 160,
      x = 100,
      y = -4
    },
    {
      h = 56,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 88,
      x = 68,
      y = 180
    },
    {
      h = 56,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 88,
      x = 204,
      y = 180
    },
    {
      h = 56,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 88,
      x = 140,
      y = 260
    },
    {
      h = 56,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 80,
      x = 28,
      y = 260
    }
  }
}