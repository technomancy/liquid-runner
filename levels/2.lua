return {
  message = "If the music is bothering you, use ctrl-m.",
  ladders = {
    {
      h = 176,
      w = 16,
      x = 200,
      y = 112
    },
    {
      h = 152,
      w = 16,
      x = 376,
      y = 176
    },
    {
      h = 296,
      ladder = true,
      w = 16,
      x = 656,
      y = 0
    }
  },
  level = 2,
  pipes = {
    {
      h = 16,
      open = false,
      pipe = true,
      tanks = {
        2,
        1
      },
      w = 112,
      x = 208,
      y = 200
    },
    {
      h = 16,
      open = false,
      pipe = true,
      tanks = {
        2,
        3
      },
      w = 112,
      x = 384,
      y = 304
    }
  },
  platforms = {
    {
      h = 16,
      w = 96,
      x = 608,
      y = 296
    },
    {
      h = 16,
      w = 16,
      x = 280,
      y = 176
    },
    {
      h = 16,
      w = 16,
      x = 456,
      y = 296
    }
  },
  tanks = {
    {
      h = 192,
      level = 158,
      original_level = 158,
      tank = true,
      thick = 16,
      w = 168,
      x = 64,
      y = 112
    },
    {
      h = 168,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 112,
      x = 296,
      y = 176
    },
    {
      h = 88,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 152,
      x = 472,
      y = 296
    }
  }
}
