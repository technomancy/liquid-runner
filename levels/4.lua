return {
  ladders = {
    {
      h = 80,
      ladder = true,
      w = 16,
      x = 240,
      y = 96
    },
    {
      h = 56,
      ladder = true,
      w = 16,
      x = 200,
      y = 192
    },
    {
      h = 144,
      ladder = true,
      w = 16,
      x = 96,
      y = 88
    },
    {
      h = 96,
      ladder = true,
      w = 16,
      x = 352,
      y = 144
    },
    {
      h = 176,
      ladder = true,
      w = 16,
      x = 456,
      y = 0
    }
  },
  level = 4,
  pipes = {
    {
      h = 16,
      open = false,
      pipe = true,
      tanks = {
        2,
        1
      },
      w = 88,
      x = 164,
      y = 140
    },
    {
      h = 16,
      open = false,
      pipe = true,
      tanks = {
        3,
        1
      },
      w = 88,
      x = 168,
      y = 264
    },
    {
      h = 16,
      open = false,
      pipe = true,
      tanks = {
        2,
        4
      },
      w = 96,
      x = 296,
      y = 160
    }
  },
  platforms = {
    {
      h = 16,
      platform = true,
      w = 88,
      x = 112,
      y = 88
    }
  },
  start = {
    x = 80,
    y = 80
  },
  tanks = {
    {
      h = 240,
      level = 149,
      original_level = 149,
      tank = true,
      thick = 16,
      w = 120,
      x = 76,
      y = 100
    },
    {
      h = 104,
      level = 83,
      original_level = 83,
      tank = true,
      thick = 16,
      w = 120,
      x = 220,
      y = 100
    },
    {
      h = 104,
      level = 7,
      original_level = 7,
      tank = true,
      thick = 16,
      w = 128,
      x = 220,
      y = 244
    },
    {
      h = 80,
      level = 5,
      original_level = 5,
      tank = true,
      thick = 16,
      w = 120,
      x = 368,
      y = 144
    }
  }
}