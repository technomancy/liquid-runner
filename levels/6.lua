return {
  ladders = {
    {
      h = 96,
      ladder = true,
      w = 16,
      x = 176,
      y = 96
    },
    {
      h = 216,
      ladder = true,
      w = 16,
      x = 280,
      y = 0
    }
  },
  level = 6,
  pipes = {
    {
      h = 64,
      open = true,
      pipe = true,
      start_open = true,
      tanks = {
        3,
        2
      },
      w = 16,
      x = 64,
      y = 256
    },
    {
      h = 56,
      open = false,
      pipe = true,
      tanks = {
        1,
        2
      },
      w = 16,
      x = 160,
      y = 176
    },
    {
      h = 16,
      open = false,
      pipe = true,
      tanks = {
        4,
        2
      },
      w = 80,
      x = 288,
      y = 240
    }
  },
  platforms = {
    {
      h = 56,
      platform = true,
      w = 16,
      x = 80,
      y = 200
    },
    {
      h = 136,
      platform = true,
      w = 16,
      x = 264,
      y = 80
    },
    {
      h = 16,
      platform = true,
      w = 32,
      x = 208,
      y = 192
    },
    {
      h = 16,
      platform = true,
      w = 56,
      x = 104,
      y = 32
    },
    {
      h = 16,
      platform = true,
      w = 56,
      x = 104,
      y = 72
    },
    {
      h = 16,
      platform = true,
      w = 56,
      x = 104,
      y = 112
    }
  },
  start = {
    x = 120,
    y = 56
  },
  tanks = {
    {
      h = 104,
      level = 88,
      original_level = 88,
      tank = true,
      thick = 16,
      w = 208,
      x = 0,
      y = 104
    },
    {
      h = 64,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 264,
      x = 48,
      y = 216
    },
    {
      h = 48,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 144,
      x = 40,
      y = 296
    },
    {
      h = 120,
      level = 97,
      original_level = 97,
      tank = true,
      thick = 16,
      w = 200,
      x = 336,
      y = 160
    }
  }
}