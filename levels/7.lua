return {
  ladders = {
    {
      h = 104,
      ladder = true,
      w = 16,
      x = 304,
      y = 104
    },
    {
      h = 24,
      ladder = true,
      w = 16,
      x = 512,
      y = 432
    },
    {
      h = 32,
      ladder = true,
      w = 16,
      x = 512,
      y = 352
    },
    {
      h = 56,
      ladder = true,
      w = 16,
      x = 496,
      y = 248
    },
    {
      h = 96,
      ladder = true,
      w = 16,
      x = 504,
      y = 144
    },
    {
      h = 56,
      ladder = true,
      w = 16,
      x = 88,
      y = 352
    },
    {
      h = 120,
      ladder = true,
      w = 16,
      x = 24,
      y = 408
    },
    {
      h = 120,
      ladder = true,
      w = 16,
      x = 856,
      y = 256
    },
    {
      h = 96,
      ladder = true,
      w = 16,
      x = 448,
      y = 160
    },
    {
      h = 40,
      ladder = true,
      w = 16,
      x = 504,
      y = 512
    },
    {
      h = 216,
      ladder = true,
      w = 16,
      x = 192,
      y = 176
    },
    {
      h = 184,
      ladder = true,
      w = 16,
      x = 64,
      y = 0
    }
  },
  level = 7,
  pipes = {
    {
      h = 200,
      open = false,
      pipe = true,
      tanks = {
        1,
        2
      },
      w = 16,
      x = 240,
      y = 224
    },
    {
      h = 80,
      open = false,
      pipe = true,
      tanks = {
        3,
        4
      },
      w = 16,
      x = 656,
      y = 104
    },
    {
      h = 176,
      open = false,
      pipe = true,
      tanks = {
        7,
        6
      },
      w = 16,
      x = 808,
      y = 376
    },
    {
      h = 16,
      open = false,
      pipe = true,
      tanks = {
        6,
        5
      },
      w = 384,
      x = 160,
      y = 552
    },
    {
      h = 88,
      open = false,
      pipe = true,
      tanks = {
        1,
        9
      },
      w = 16,
      x = 304,
      y = 320
    },
    {
      h = 160,
      open = false,
      pipe = true,
      tanks = {
        8,
        1
      },
      w = 16,
      x = 144,
      y = 224
    }
  },
  platforms = {
    {
      h = 16,
      platform = true,
      w = 88,
      x = 0,
      y = 352
    },
    {
      h = 16,
      platform = true,
      w = 416,
      x = 104,
      y = 496
    },
    {
      h = 32,
      platform = true,
      w = 16,
      x = 536,
      y = 496
    },
    {
      h = 296,
      platform = true,
      w = 16,
      x = 544,
      y = 232
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 496,
      y = 464
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 528,
      y = 432
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 496,
      y = 408
    },
    {
      h = 152,
      platform = true,
      w = 16,
      x = 480,
      y = 216
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 496,
      y = 352
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 528,
      y = 256
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 488,
      y = 216
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 520,
      y = 200
    },
    {
      h = 16,
      platform = true,
      w = 64,
      x = 40,
      y = 408
    },
    {
      h = 16,
      platform = true,
      w = 16,
      x = 528,
      y = 328
    },
    {
      h = 16,
      platform = true,
      w = 32,
      x = 808,
      y = 256
    },
    {
      h = 16,
      platform = true,
      w = 312,
      x = 184,
      y = 536
    },
    {
      h = 16,
      platform = true,
      w = 56,
      x = 464,
      y = 168
    },
    {
      h = 16,
      platform = true,
      w = 120,
      x = 360,
      y = 296
    },
    {
      h = 16,
      platform = true,
      w = 40,
      x = 336,
      y = 232
    }
  },
  start = {
    x = 448,
    y = 480
  },
  tanks = {
    {
      h = 120,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 392,
      x = 104,
      y = 352
    },
    {
      h = 136,
      level = 120,
      original_level = 120,
      tank = true,
      thick = 16,
      w = 112,
      x = 224,
      y = 112
    },
    {
      h = 80,
      level = 52,
      original_level = 52,
      tank = true,
      thick = 16,
      w = 320,
      x = 520,
      y = 152
    },
    {
      h = 104,
      level = 88,
      original_level = 88,
      tank = true,
      thick = 16,
      w = 520,
      x = 576,
      y = 24
    },
    {
      h = 64,
      level = 7,
      original_level = 7,
      tank = true,
      thick = 16,
      w = 168,
      x = 32,
      y = 560
    },
    {
      h = 32,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 432,
      x = 496,
      y = 544
    },
    {
      h = 176,
      level = 0,
      original_level = 0,
      tank = true,
      thick = 16,
      w = 96,
      x = 792,
      y = 256
    },
    {
      h = 72,
      level = 56,
      original_level = 56,
      tank = true,
      thick = 16,
      w = 128,
      x = 64,
      y = 184
    },
    {
      h = 96,
      level = 80,
      original_level = 80,
      tank = true,
      thick = 16,
      w = 104,
      x = 272,
      y = 248
    }
  }
}