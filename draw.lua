local utils = require("utils")
local editor = require("polywell")
local lume = require("lume")

local offset = function(p, w, h)
   local x, y = 0,0
   if(p.x > w) then x = w-p.x end
   if(p.y > h) then y = h-p.y end
   return x, y
end

local draw_pipe = function(p)
   if(p.error) then
      love.graphics.setColor(0.9, 0.1, 0.08)
      for _,t in pairs(p.tanks) do
         love.graphics.setColor(0.9, 0.1, 0.08, 0.3)
         love.graphics.rectangle("fill", utils.xywh(state.tanks[t]))
      end
   elseif(p.open) then
      love.graphics.setColor(0.1, 0.6, 0.1)
   else
      love.graphics.setColor(0.1, 0.5, 0.1)
   end
   local x,y,w,h = utils.xywh(p)
   love.graphics.rectangle("fill", x,y,w,h)
   love.graphics.setColor(0,0,0,0.2)
   if(p.h > p.w) then -- shadow
      love.graphics.rectangle("fill", x+w/2,y,w/2,h)
   else
      love.graphics.rectangle("fill", x,y+h/2,w,h/2)
   end
   love.graphics.setColor(0,0,0,0.3)
   if(p.h > p.w) then -- shadow
      love.graphics.rectangle("fill", x+w-w/8,y,w/8,h)
   else
      love.graphics.rectangle("fill", x,y+h-h/8,w,h/8)
   end
   love.graphics.setColor(1,1,1,0.2)
   if(p.h > p.w) then -- shine
      love.graphics.rectangle("fill", x+w/8,y,w/8,h)
   else
      love.graphics.rectangle("fill", x,y+h/8,w,h/8)
   end
end

local imgs = {
   still_right = {love.graphics.newImage("assets/still-right.png")},
   still_left = {love.graphics.newImage("assets/still-left.png")},
   right = { love.graphics.newImage("assets/right1.png"),
             love.graphics.newImage("assets/right2.png"),
   },
   left = { love.graphics.newImage("assets/left1.png"),
            love.graphics.newImage("assets/left2.png"),
   },
   jump_left = {love.graphics.newImage("assets/jump-left.png")},
   jump_right = {love.graphics.newImage("assets/jump-right.png")},
   swim_left = {love.graphics.newImage("assets/swim-left1.png"),
                love.graphics.newImage("assets/swim-left2.png"),
                love.graphics.newImage("assets/swim-left3.png"),
                love.graphics.newImage("assets/swim-left4.png"),},
   swim_right = {love.graphics.newImage("assets/swim-right1.png"),
                 love.graphics.newImage("assets/swim-right2.png"),
                 love.graphics.newImage("assets/swim-right3.png"),
                 love.graphics.newImage("assets/swim-right4.png")},
   ladder = {love.graphics.newImage("assets/ladder1.png"),
             love.graphics.newImage("assets/ladder2.png"),
             love.graphics.newImage("assets/ladder3.png"),
             love.graphics.newImage("assets/ladder2.png"),},
   ladder_left = {love.graphics.newImage("assets/ladder3.png")},
   ladder_right = {love.graphics.newImage("assets/ladder1.png")},
}
for _,is in pairs(imgs) do for _,i in pairs(is) do
      i:setFilter("nearest", "nearest")
end end

local frame_dt = 1

local draw_player = function(p)
   if(editor.current_mode_name() == "level_edit") then
      love.graphics.setColor(0.9, 0.9, 0.9)
      love.graphics.rectangle("fill", state.world:getRect(p))
   else
      local x, y = state.world:getRect(p)
      local frames = imgs[p.action]
      frame_dt = frame_dt + love.timer.getDelta() * 4
      if(math.floor(frame_dt) > #frames) then
         frame_dt = 1
      end
      local img = frames[math.floor(frame_dt)]
      love.graphics.draw(img, x, y, nil, nil, nil, 4)
   end
end

local canvases = {}

local k = function(s) return s.x .. s.y .. s.w .. s.h end

local draw_moss = function(s)
   local length = 0
   local width = 0
   for i=1,s.w do
      if(love.math.random(8) > 7) then
         width = 1
      end
      love.graphics.setColor(0.01,0.3,0.01,love.math.randomNormal(0.6, 0.1))
      if(width > 0 and width < 5) then
         love.graphics.line(i,0,i,length)
         length = length + love.math.randomNormal(1, 1)
         width = width + 1
      elseif(width < 12 and length > 0) then
         love.graphics.line(i,0,i,length)
         length = length - love.math.randomNormal(1, 1)
         width = width + 1
      elseif(length <= 0) then
         width = 0
      end
   end
end

local make_concrete = function(s)
   love.graphics.push()
   love.graphics.origin()
   local c = love.graphics.newCanvas(s.w, s.h)
   c:renderTo(function()
         love.graphics.setColor(0.3, 0.3, 0.3)
         love.graphics.rectangle("fill", 0, 0, s.w, s.h)
         draw_moss(s)
   end)
   love.graphics.pop()
   canvases[k(s)] = c
   return c
end

local draw_concrete = function(s)
   if(editor.current_mode_name() == "default") then
      local c = canvases[k(s)] or make_concrete(s)
      love.graphics.setColor(1,1,1)
      love.graphics.draw(c, s.x, s.y)
   else
      love.graphics.setColor(0.2, 0.2, 0.2)
      love.graphics.rectangle("fill", s.x, s.y, s.w, s.h)
   end
end

local scale,w,h,canvas = 2

local resize = function(fs, skip_mode)
   if(fs) then
      w,h = love.window.getDesktopDimensions()
   else
      if(love.filesystem.getInfo("window")) then
         local wh = lume.split(love.filesystem.read("window"), " ")
         w, h = tonumber(wh[1]), tonumber(wh[2])
      else
         w, h = 800, 600
      end
   end
   if(h > 1000) then scale = 4 end

   canvas = love.graphics.newCanvas(math.max(state.w, w), math.max(state.h, h))
   canvas:setFilter("nearest", "nearest")
   canvases = {}
   if(not skip_mode) then
      love.window.setMode(w, h, {fullscreen=fs, fullscreentype="desktop",
                                 resizable=not fs})
   end
end

return {
   draw = function()
      if(state.win_scale) then
         love.graphics.scale(state.win_scale)
         love.graphics.print("You win", 64, 64)
         if(state.win_scale < 20) then
            state.win_scale = state.win_scale * 1.01
         end
         return
      end

      love.graphics.setCanvas(canvas)
      love.graphics.clear()
      love.graphics.push()
      love.graphics.translate(offset(state.p, w*0.7/scale, h*0.7/scale))
      -- player
      draw_player(state.p)
      -- platforms
      for _,p in pairs(state.platforms) do
         draw_concrete(p)
      end
      -- tanks
      for _,t in pairs(state.tanks) do
         for _,t2 in pairs(utils.tank_rects(t)) do
            local x,y,tw,th = unpack(t2)
            draw_concrete({x=x,y=y,w=tw,h=th})
         end
      end
      -- pipes
      for _,p in pairs(state.pipes) do
         draw_pipe(p)
      end
      -- liquid
      love.graphics.setColor(0.2, 0.2, 1, 0.3)
      for _,t in pairs(state.tanks) do
         love.graphics.rectangle("fill", unpack(utils.liquid_rect(t)))
      end
      -- spills
      for _,s in pairs(state.spills) do
         love.graphics.rectangle("fill", utils.xywh(s))
         love.graphics.rectangle("fill", s.on.x, s.on.y-2, s.on.w, 2)
      end
      -- ladders
      love.graphics.setColor(1, 0, 1)
      for _,l in pairs(state.ladders) do
         love.graphics.line(l.x, l.y, l.x, l.y + l.h)
         love.graphics.line(l.x + l.w, l.y, l.x + l.w, l.y + l.h)
         for i=l.y + 5, l.y + l.h - 5, 10 do
            love.graphics.line(l.x, i, l.x + l.w, i)
         end
      end
      -- message
      love.graphics.setColor(1,1,1)
      love.graphics.pop()
      love.graphics.print(state.message, 10, h/scale - 25)
      love.graphics.setCanvas()
      love.graphics.draw(canvas, 0, 0, 0, scale, scale)
   end,
   resize = resize,
}
