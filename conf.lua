love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "Liquid Runner", "liquid-runner"
   t.modules.joystick, t.modules.physics = false, false
   t.window.resizable = true
end
