local utils = require("utils")
local lume = require("lume")

local abs_level = function(tank)
   return tank.y + tank.h - tank.thick - tank.level
end

local pick_target = function(items, spill)
   for _,i in ipairs(lume.sort(items, "y")) do
      if(i.y > spill.y) then return i end
   end
end

local make_spill = function(source, x, y, amount, on)
   local spill = {x=x,y=y-1,amount=amount,w=2,h=state.h,source=source,
                  on=on or source}
   local items = state.spillable_world:querySegment(spill.x, spill.y,
                                                    spill.x + spill.w,
                                                    spill.y + spill.h)
   spill.target = pick_target(items, spill)
   if(spill.target and spill.target.tank) then
      -- TODO: if you land on the edge of a tank, treat it like a platform
      spill.h = spill.target.y + spill.target.h - spill.target.thick - spill.y
   elseif(spill.target) then
      spill.h = spill.target.y - spill.y
   end
   return spill
end

local overflow = function(tank)
   local amount = tank.level - (tank.h - tank.thick)
   if(tank.spills) then
      tank.spills.left.amount = amount
      tank.spills.right.amount = amount
   else
      local spill_left = make_spill(tank, tank.x - 2, tank.y, amount/2)
      local spill_right = make_spill(tank, tank.x + tank.w, tank.y, amount/2)
      table.insert(state.spills, spill_left)
      table.insert(state.spills, spill_right)
      tank.spills = {left = spill_left, right = spill_right}
   end
end

local split_spills = function(tank, platform, amount)
   if(platform.spills) then
      platform.spills.left.amount = amount
      platform.spills.right.amount = amount
   else
      local spill_left = make_spill(tank, platform.x - 2, platform.y,
                                    amount/2, platform)
      local spill_right = make_spill(tank, platform.x + platform.w,
                                     platform.y, amount/2, platform)
      table.insert(state.spills, spill_left)
      table.insert(state.spills, spill_right)
      platform.spills = {left = spill_left, right = spill_right}
   end
end

local move_liquid = function(from, to, amount)
   if(from.level - amount < 0) then
      if(to.level) then to.level = to.level + from.level end
      from.level = 0
   else
      from.level = from.level - amount
      if(to.level) then to.level = to.level + amount * (from.w / to.w) end
   end
   if(to.void) then
      return
   elseif(not to.level) then -- spilling onto platform
      split_spills(from, to, amount)
   elseif(to.level > to.h - to.thick) then -- spilling into tank
      overflow(to)
   end
   if(state.liquid_world:hasItem(from)) then
      state.liquid_world:remove(from)
   end
   if(from.level and from.level > 0) then
      state.liquid_world:add(from, unpack(utils.liquid_rect(from)))
   end
   if(state.liquid_world:hasItem(to)) then
      state.liquid_world:remove(to)
   end
   if(to.level and to.level > 0) then
      state.liquid_world:add(to, unpack(utils.liquid_rect(to)))
   end
end

local update_pipe = function(pipe)
   if(not pipe.open) then return end
   local from, to = state.tanks[pipe.tanks[1]], state.tanks[pipe.tanks[2]]
   local from_level, to_level = abs_level(from), abs_level(to)
   if(to_level < from_level) then
      from, from_level, to, to_level = to, to_level, from, from_level
   end
   if(math.abs(to_level - from_level) < 0.5) then return end
   if(from_level > pipe.y + pipe.h) then return end
   local amount = (pipe.y + math.min(pipe.w, pipe.h) - from_level) *
      state.flow_factor
   move_liquid(from, to, amount)
end

local update_spill = function(spill, i)
   if(spill.source.level <= spill.source.h - spill.source.thick) then
      spill.done = spill.done or 10
      spill.on.spills = nil
   end
   if(spill.done and spill.done < 1) then
      table.remove(state.spills, i)
   elseif(spill.done) then
      spill.done = spill.done - 1
   else
      if(spill.target) then
         move_liquid(spill.source, spill.target, spill.amount)
      else
         move_liquid(spill.source, {void=true,level=0,w=1}, spill.amount)
      end
   end
end

return {
   update_pipe = update_pipe,
   update_spill = update_spill,
}
