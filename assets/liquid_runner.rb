# source for liquid_runner.ogg using Sonic Pi v2.10
# avconv -ss $OFFSET -i liquid_runner.wav assets/liquid_runner.ogg

use_synth :mod_pulse
use_synth_defaults amp: 4, attack: 0.125, release: 0.125
use_bpm 80

with_fx :reverb do
  with_fx :bitcrusher do
    with_fx :slicer do
      times = [0.25, 0.5, 0.25, 0.5]
      notes = [:a4, :g4, :a4, :g4, :a4, :g4, :a4, :e4] +
        [:a4, :g4, :a4, :b4, :c5, :b4, :c5, :b4] +
        [:a4, :g4, :a4, :g4, :a4, :g4, :a4, :e4] +
        [:d4, :d4, :d4, :d4, :c4, :d4, :e4, :g4]
      timing = (times * 8) +
        [0.125, 0.125, 0.125, 0.125, 0.25, 0.25, 0.25, 0.5]
      live_loop :e do
        play_pattern_timed notes, timing
      end
    end
  end
end

live_loop :drums do
  sample :drum_bass_hard, amp: 3
  sleep 0.5
  sample :drum_bass_hard, amp: 3
  sample :drum_cymbal_closed, amp: 3
  sleep 0.5
end

use_synth :subpulse
use_synth_defaults amp: 5
with_fx :reverb, room: 0.8, mix: 0.6 do
  with_fx :ring_mod do
    live_loop :bass do
      play_pattern_timed [:d3, :c3, :d3, :c3, :a3, :g3, :a3, :g3],
        ring(0.25, 0.5)
    end
  end
end
