# Liquid Runner

Swim to escape by manipulating the liquids around you!

* Arrows: move
* Space: activate pipes
* Ctrl-r: restart
* Ctrl-m: toggle music
* Ctrl-f: toggle fullscreen

![screenshot](https://p.hagelb.org/liquid-runner.png)

Download [releases](https://technomancy.itch.io/liquid-runner)

Press `f2` to toggle the level editor.

* Ctrl-arrows: move
* Alt-arrows: resize
* p: create platform
* l: create ladder
* t: create tank
* i: create pipe
* [ and ]: adjust liquid level of tank
* - and =: go forward and backward a level
* Ctrl-z: undo
* Ctrl-v: revert level to last saved

Note that pipes must be connected to two tanks to function.

You can package up levels you've created to share with others! The
location they are saved to varies by platform:

* Windows: `C:\Users\user\AppData\Roaming\LOVE\liquid-runner\levels` or `%appdata%\LOVE\liquid-runner\levels`
* Linux: `$XDG_DATA_HOME/love/liquid-runner/levels` or `~/.local/share/love/liquid-runner/levels`
* Mac: `/Users/user/Library/Application Support/LOVE/liquid-runner/levels`

## Meta

This game was written using the
[Polywell](https://gitlab.com/technomancy/polywell) in-game text
editor; you can activate it using `ctrl-enter`.

## Licenses

Original code, prose, music, and images copyright © 2016 Phil Hagelberg, Noah Hagelberg, and Zach Hagelberg

Distributed under the GNU General Public License version 3 or later; see file LICENSE.

Uses 3rd-party libraries [Lume](https://github.com/rxi/lume),
[Serpent](https://github.com/pkulchenko/serpent), and
[bump](https://github.com/kikito/bump.lua) licensed under the MIT license.
