local editor = require("polywell")
local bump = require("bump.bump")
local lume = require("lume")
local serpent = require("serpent")

local utils = require("utils")
local draw = require("draw")
local liquid = require("liquid")

local serpent_opts = {maxlevel=8,maxnum=64,nocode=true,comment=false}
local pps = function(x) return serpent.block(x, serpent_opts) end
pp = function(x) print(pps(x)) end

local music = love.audio.newSource("assets/liquid_runner.ogg", "stream")
music:setLooping(true)

local load_level = function(level)
   return love.filesystem.load("levels/" .. level .. ".lua")()
end

local write_level = function(level_data)
   if(love.filesystem.getRealDirectory("main.lua"):find(".love$")) then
      love.filesystem.createDirectory("levels")
      local filename = "/levels/" .. level_data.level .. ".lua"
      assert(love.filesystem.write(filename, "return " .. pps(level_data)))
      state.message = "Saved to "..love.filesystem.getSaveDirectory()..filename
   else
      local filename = "levels/" .. level_data.level .. ".lua"
      local f = assert(io.open(filename, "w"),
                       "Could not write level " .. filename)
      assert(f:write("return " .. pps(level_data)))
      f:close()
   end
end

local level_exists = function(level)
   return love.filesystem.getInfo("levels/" .. level .. ".lua")
end

local restart = function(level, message)
   if(level == 0) then level = 1 end
   state = {
      level = level or (state and state.level) or 1,
      gravity = 1024, flow_factor = 1/64,
      liquid_gravity = 256, liquid_max_dy = 64,
      p = {name="player", x=100,y=100,dx=0,dy=0,w=8,h=16,
           speed=86, jump=256, swim_speed = 64,
           action = "still_right", last_dir = "right",},
      message = message or "Liquid Runner!",
      world = bump.newWorld(32),
      ladder_world = bump.newWorld(32),
      liquid_world = bump.newWorld(32),
      pipe_world = bump.newWorld(32),
      spillable_world = bump.newWorld(32),
      platforms = {}, ladders = {}, tanks = {}, pipes = {}, spills = {},
      w = 2048, h = 2048,
   }

   if(not level_exists(state.level)) then
      state.level, state.win_scale = state.level - 1, 1
   end

   lume.extend(state, load_level(state.level))
   lume.extend(state.p, state.start)

   local expand_dimensions = function(i)
      state.w,state.h = math.max(state.w, i.x+i.w), math.max(state.h, i.y+i.h)
   end

   state.world:add(state.p, utils.xywh(state.p))
   state.ladder_world:add(state.p, utils.xywh(state.p))
   state.liquid_world:add(state.p, utils.xywh(state.p))
   state.pipe_world:add(state.p, utils.xywh(state.p))

   for _,p in pairs(state.platforms) do
      state.world:add(p, utils.xywh(p))
      state.spillable_world:add(p, utils.xywh(p))
      expand_dimensions(p)
   end

   for _,tank in pairs(state.tanks) do
      for _,t in pairs(utils.tank_rects(tank)) do
         state.world:add(t, unpack(t))
      end
      state.spillable_world:add(tank, utils.xywh(tank))
      if(tank.level > 0) then
         state.liquid_world:add(tank, unpack(utils.liquid_rect(tank)))
      end
      expand_dimensions(tank)
   end

   for _,l in pairs(state.ladders) do
      state.ladder_world:add(l, utils.xywh(l))
      expand_dimensions(l)
   end

   for _,p in pairs(state.pipes) do
      state.pipe_world:add(p, utils.xywh(p))
   end

   if(not music:isPlaying() and not love.filesystem.getInfo("mute")) then
      music:play()
   end
end

if(not state) then restart() end

love.draw = lume.fn(editor.draw, draw.draw)

local check_world = function(x, y, w)
   w:update(state.p, x, y)
   local _,_,cols,len = w:check(state.p, x, y)
   return len > 0 and cols
end

local next_level = function()
   restart(state.level + 1)
end

love.update = function(dt)
   if(state.win) then return end
   if(editor.current_mode_name() == "default") then
      local x, y = state.world:getRect(state.p)
      local on_ladder = check_world(x, y, state.ladder_world)
      local in_liquid = check_world(x, y, state.liquid_world)
      state.p.action = "still_" .. state.p.last_dir

      if(on_ladder) then
         state.p.dy = 0
         if(love.keyboard.isDown("up")) then
            y = y - state.p.speed * dt
         elseif(love.keyboard.isDown("down")) then
            y = y + state.p.speed * dt
         end
      elseif(in_liquid) then
         state.p.dy = state.p.dy + state.liquid_gravity * dt
         if(state.p.dy > state.liquid_max_dy) then
            state.p.dy = state.liquid_max_dy
         end
      else
         state.p.dy = state.p.dy + state.gravity * dt
      end

      y = y + state.p.dy * dt

      local speed = state.p.speed
      if(in_liquid) then speed = state.p.swim_speed end
      if(love.keyboard.isDown("left")) then
         state.p.action = in_liquid and "swim_left" or "left"
         state.p.last_dir = "left"
         x = x - speed * dt
      elseif(love.keyboard.isDown("right")) then
         state.p.action = in_liquid and "swim_right" or "right"
         state.p.last_dir = "right"
         x = x + speed * dt
      end

      state.p.x, state.p.y = state.world:move(state.p, x, y)

      local on_ground = state.p.y ~= y
      if(on_ground) then
         if(love.keyboard.isDown("up")) then
            if(not on_ladder and not in_liquid) then
               state.p.action = "jump_" .. state.p.last_dir
               state.p.dy = - state.p.jump
            elseif(in_liquid) then
               state.p.dy = -state.p.swim_speed * 2
            end
         else
            state.p.dy = 0
         end
         if(love.keyboard.isDown("left")) then
            state.p.action = "left"
         elseif(love.keyboard.isDown("right")) then
            state.p.action = "right"
         end
      elseif((love.keyboard.isDown("up"))
             and in_liquid and not on_ladder) then
         state.p.action = "swim_" .. state.p.last_dir
         state.p.dy = math.max(-state.p.swim_speed,
                               state.p.dy - state.p.swim_speed)
      elseif((love.keyboard.isDown("up")) and not on_ladder) then
         state.p.action = "jump_" .. state.p.last_dir
      elseif(on_ladder) then
         if(love.keyboard.isDown("up", "down")) then
            state.p.action = "ladder"
         else
            state.p.action = "ladder_" .. state.p.last_dir
         end
      end
      if(state.p.y > state.h) then
         state.message = "Oops! Press ctrl-r to restart."
      end
      for _,p in pairs(state.pipes) do liquid.update_pipe(p) end
      for i,s in lume.ripairs(state.spills) do liquid.update_spill(s, i) end
      if(state.p.y < 0) then next_level() end
   end
end

love.resize = function(w,h)
   love.filesystem.write("window", w .. " " .. h)
   draw.resize(love.filesystem.getInfo("fullscreen"), true)
end

draw.resize(love.filesystem.getInfo("fullscreen"))

-- TODO:
-- * more levels
-- * pumps?
-- * boxes?

love.load = function()
   love.keyreleased = function() end
   love.keypressed = editor.handle_key
   love.textinput = editor.handle_textinput
   love.wheelmoved = editor.handle_wheel

   love.graphics.setFont(love.graphics.newFont("polywell/inconsolata.ttf", 16))
   love.keyboard.setKeyRepeat(true)

   local init_file = os.getenv("HOME") .. "/.polywell/init.lua"
   if(not pcall(dofile, init_file)) then
      local chunk = assert(love.filesystem.load("polywell/config/init.lua"))
      chunk()
   end

   editor.fs = require("polywell.fs")(os.getenv("PWD"))
   editor.change_buffer()
end

editor.bind("default", "ctrl-o", editor.find_file)
editor.bind("default", "ctrl-enter", function()
               editor.change_buffer("*console*")
end)
editor.bind("default", "ctrl-q", function() love.event.quit() end)
editor.bind("default", "ctrl-alt-r", editor.reload)
editor.bind("default", "ctrl-r", restart)

editor.bind("default", "ctrl-m", function()
               if(love.filesystem.getInfo("mute")) then
                  love.filesystem.remove("mute")
                  music:play()
               else
                  love.filesystem.write("mute", "true")
                  music:stop()
               end
end)

editor.bind("default", "ctrl-f", function()
               if(love.filesystem.getInfo("fullscreen")) then
                  love.filesystem.remove("fullscreen")
               else
                  love.filesystem.write("fullscreen", "true")
               end
               draw.resize(love.filesystem.getInfo("fullscreen"))
end)

local pipe_toggle = function()
   local on_pipe = check_world(state.p.x, state.p.y, state.pipe_world)
   if(on_pipe) then
      on_pipe[1].other.open = not on_pipe[1].other.open
   end
end

editor.bind("default", "space", pipe_toggle)
editor.bind("default", " ", pipe_toggle)

require("level_edit")
local save_level = function()
   for _,t in pairs(state.tanks) do
      t.level = t.original_level or t.level or 0
   end
   for _,p in pairs(state.pipes) do
      p.open, p.error = (p.start_open or false), nil
      if(#p.tanks < 2) then
         state.message = "Disconnected pipe; cannot save."
         p.error = true
         return false
      elseif(#p.tanks > 2) then
         state.message = "Pipe connected to too many tanks; cannot save."
         p.error = true
         return false
      end
   end
   local level = lume.pick(state, "level", "platforms",
                           "ladders", "tanks", "pipes", "start")
   level.message = level.original_message or level.message
   write_level(level)
   return true
end

local deactivate_level_editor = function()
   if(save_level()) then
      editor.change_buffer()
      restart(state.level, state.message)
   end
end
editor.bind("level_edit", "`", deactivate_level_editor)
editor.bind("level_edit", "f2", deactivate_level_editor)

editor.bind("level_edit", "ctrl-v", function() restart(state.level) end)

editor.bind("level_edit", "=", function()
               if(save_level()) then
                  if(not level_exists(state.level + 1)) then
                     write_level({ level=state.level + 1 })
                  end
                  restart(state.level + 1)
                  state.message = "On to level " .. state.level
               end
end)
editor.bind("level_edit", "-", function()
               if(save_level()) then
                  restart(state.level - 1)
               end
end)
