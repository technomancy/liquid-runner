return {
   xywh = function(item)
      return item.x, item.y, item.w, item.h
   end,

   tank_rects = function(t)
      return {
         {t.x, t.y, t.thick, t.h},
         {t.x + t.w - t.thick, t.y, t.thick, t.h},
         {t.x + t.thick, t.y + t.h - t.thick,
          math.max(t.w - t.thick * 2, t.thick), t.thick},
      }
   end,

   liquid_rect = function(t)
      return {t.x + t.thick, t.y + t.h - t.thick - t.level,
              t.w - t.thick * 2, t.level}
   end,
}
